var gulp         = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    changed      = require('gulp-changed'),
    coffee       = require('gulp-coffee'),
    coffeelint   = require('gulp-coffeelint'),
    concat       = require('gulp-concat'),
    csso         = require('gulp-csso'),
    expect       = require('gulp-expect-file'),
    filter       = require('gulp-filter'),
    gutil        = require('gulp-util'),
    jade         = require('gulp-jade'),
    jshint       = require('gulp-jshint'),
    less         = require('gulp-less'),
    livereload   = require('gulp-livereload'),
    minifyCSS    = require('gulp-minify-css'),
    ngAnnotate   = require('gulp-ng-annotate'),
    prettifyHtml = require('gulp-html-prettify'),
    rename       = require('gulp-rename'),
    sync         = require('gulp-sync')(gulp),
    uglify       = require('gulp-uglify');

var hiddenFiles  = '**/_*.*',
    ignoredFiles = '!' + hiddenFiles;

var vendor = {
    base: {
        source     : require('./vendor.base.json'),
        destination: '../public/js',
        name       : 'vendor.min.js'
    },
    app : {
        source     : require('./vendor.json'),
        destination: '../public/vendor'
    }
};

var source  = {
        bootstrap: {
            source     : 'bower_components/bootstrap/less/**/*.less',
            destination: 'less/bootstrap'
        },
        template : {
            main: {
                files: ['jade/index.jade'],
                watch: ['jade/index.jade', hiddenFiles]
            },
            view: {
                files: ['jade/views/*.jade', 'jade/views/**/*.jade', ignoredFiles],
                watch: ['jade/views/**/*.jade']
            }
        },
        script   : {
            coffee: {
                files: [
                    'coffee/spectre.init.coffee',
                    'coffee/modules/*.coffee',
                    'coffee/modules/controllers/*.coffee',
                    'coffee/modules/directives/*.coffee',
                    'coffee/modules/factories/*.coffee',
                    'coffee/modules/providers/*.coffee',
                    'coffee/modules/services/*.coffee',
                    'coffee/modules/filters/*.coffee'
                ],
                watch: ['coffee/**/*.coffee']
            }
        },
        style    : {
            app   : {
                main : ['less/spectre.less', '!less/themes/*.less'],
                dir  : 'less',
                watch: ['less/*.less', 'less/**/*.less', '!less/themes/*.less']
            },
            themes: {
                main : ['less/themes/*.less', ignoredFiles],
                dir  : 'less/themes',
                watch: ['less/themes/**/*.less']
            }
        },
        translate: {
            i18n: {
                files: ['i18n/**/*.json']
            }
        }
    },
    build   = {
        script   : {
            coffee: {
                name       : 'spectre.min.js',
                destination: '../public/js'
            }
        },
        style    : '../public/css',
        template : {
            main: '../public',
            view: '../public/views'
        },
        translate: '../public/i18n'
    },
    options = {
        coffeelint  : {
            indentation                 : {level: 'warn', value: 4},
            line_endings                : {value: 'unix'},
            max_line_length             : {value: 120},
            no_unnecessary_double_quotes: {level: 'warn'},
            prefer_english_operator     : {level: 'error'}
        },
        livereload  : {
            port: 35729
        },
        prettifyHtml: {
            indent_char: ' ',
            indent_size: 2,
            unformatted: ['a', 'sub', 'sup', 'b', 'i', 'u']
        }
    };

gulp.task('script:app', ['script:coffee']);
gulp.task('script:vendor', ['script:vendor:base', 'script:vendor:app']);

// Vendor Scripts
gulp.task('script:vendor:base', function () {
    return gulp.src(vendor.base.source)
        .pipe(expect(vendor.base.source))
        //.pipe(uglify())
        .pipe(concat(vendor.base.name))
        .pipe(gulp.dest(vendor.base.destination));
});

gulp.task('script:vendor:app', function () {
    var gulpFilter = {
        js : filter('**/*.js'),
        css: filter('**/*.css')
    };

    return gulp.src(vendor.app.source, {base: 'bower_components'})
        .pipe(expect(vendor.app.source))
        .pipe(gulpFilter.js)
        .pipe(uglify())
        .pipe(gulpFilter.js.restore())
        .pipe(gulpFilter.css)
        .pipe(csso())
        .pipe(minifyCSS())
        .pipe(gulpFilter.css.restore())
        .pipe(gulp.dest(vendor.app.destination));
});

// Script [COFFEE]
gulp.task('script:coffee', function () {
    return gulp.src(source.script.coffee.files)
        .pipe(coffeelint(options.coffeelint))
        .pipe(coffeelint.reporter())
        .pipe(coffee({bare: true}))
        .on('error', handleError)
        .pipe(concat(build.script.coffee.name))
        .pipe(ngAnnotate({add: true, remove: true, single_quotes: true}))
        .on('error', handleError)
        .pipe(gulp.dest(build.script.coffee.destination))
});

// Style [LESS]
gulp.task('style:bootstrap:vendor', function () {
    return gulp.src(source.bootstrap.source)
        .pipe(expect(source.bootstrap.source))
        .pipe(gulp.dest(source.bootstrap.destination));
});

gulp.task('style:app', function () {
    return gulp.src(source.style.app.main)
        .pipe(less({paths: [source.style.app.dir]}))
        .on('error', handleError)
        .pipe(autoprefixer())
        .on('error', handleError)
        .pipe(csso())
        .pipe(minifyCSS({keepSpecialComments: 0}))
        .pipe(rename(function (path) {
            path.basename += '.min'
        }))
        .pipe(gulp.dest(build.style))
});

gulp.task('style:theme', function () {
    return gulp.src(source.style.themes.main)
        .pipe(less({paths: [source.style.themes.dir]}))
        .on('error', handleError)
        .pipe(autoprefixer())
        .on('error', handleError)
        .pipe(csso())
        .pipe(minifyCSS({keepSpecialComments: 0}))
        .pipe(rename(function (path) {
            path.basename += '.min'
        }))
        .pipe(gulp.dest(build.style))
});

// Templates [JADE]
gulp.task('template:jade:main', function () {
    return gulp.src(source.template.main.files)
        //.pipe(changed(build.template.main, {extension: '.html'}))
        .pipe(jade())
        .on('error', handleError)
        .pipe(prettifyHtml(options.prettifyHtml))
        .pipe(gulp.dest(build.template.main));
});

gulp.task('template:jade:view', function () {
    return gulp.src(source.template.view.files)
        //.pipe(changed(build.template.main, {extension: '.html'}))
        .pipe(jade())
        .on('error', handleError)
        .pipe(prettifyHtml(options.prettifyHtml))
        .pipe(gulp.dest(build.template.view));
});

// Language
gulp.task('translate:i18n', function () {
    return gulp.src(source.translate.i18n.files)
        .pipe(gulp.dest(build.translate))
});

// Runners
gulp.task('develop', sync.sync(['default', 'watch']));
gulp.task('default', sync.sync(['script:vendor', 'script:app', 'style:theme', 'style:bootstrap:vendor', 'start']));
gulp.task('start', ['style:app', 'template:jade:main', 'template:jade:view', 'translate:i18n']);

// Watchers
gulp.task('watch', function () {
    livereload.listen(options.livereload);

    gulp.watch(source.bootstrap.source, ['style:bootstrap:vendor', 'style:app']);
    gulp.watch(source.script.coffee.watch, ['script:coffee']);
    gulp.watch(source.style.app.watch, ['style:app']);
    gulp.watch(source.style.themes.watch, ['style:theme']);
    gulp.watch(source.template.main.watch, ['template:jade:main']);
    gulp.watch(source.template.view.watch, ['template:jade:view']);
    gulp.watch(source.translate.i18n.files, ['translate:i18n']);

    gulp.watch('../public/**').on('change', function (event) {
        livereload.changed(event.path)
    });
});

// Methods
var handleError = function (error) {
    console.log(error.toString());
    this.emit('end');
};
