spectre.provider 'RouteHelpers', (APP_REQUIRES) ->
    'use strict'

    @basepath = (uri) ->
        'views/' + uri

    @resolveFor = ->
        _args = arguments
        {
        deps: ($ocLazyLoad, $q) ->
            promise = $q.when(1)
            andThen = (_arg) ->
                if typeof _arg is 'function'
                    promise.then _arg
                else
                    promise.then ->
                        whatToLoad = getRequired(_arg)
                        if not whatToLoad
#                            return $.error('Route resolve: Bad resource name [' + _arg + ']')
#                            console.error('Route resolve: Bad resource name [' + _arg + ']')
                            return
                        $ocLazyLoad.load whatToLoad

            getRequired = (name) ->
                if APP_REQUIRES.modules
                    for module in APP_REQUIRES.modules
                        if module.name and module.name is name
                            return module
                APP_REQUIRES.scripts and APP_REQUIRES.scripts[name]

            i = 0
            len = _args.length
            while i < len
                promise = andThen(_args[i])
                i++
            promise
        }

    @$get = ->

    return
