spectre.factory 'colours', (APP_COLOURS) ->
    byName: (name) ->
        APP_COLOURS[name] or '#fff'

