spectre.service 'ToggleStateService', ($rootScope) ->
    storageKeyName = 'toggleState'
    WordChecker =
        hasWord: (phrase, word) ->
            new RegExp('(^|\\s)' + word + '(\\s|$)').test phrase
        addWord: (phrase, word) ->
            if not @hasWord phrase, word
                phrase + (if phrase then ' ' else '') + word
        removeWord: (phrase, word) ->
            if @hasWord phrase, word
                phrase.replace new RegExp('(^|\\s)*' + word + '(\\s|$)*', 'g'), ''

    {
        addState: (className) ->
            data = angular.fromJson $rootScope.$storage[storageKeyName]
            if not data
                data = className
            else
                data = WordChecker.addWord data, className
            $rootScope.$storage[storageKeyName] = angular.toJson data
            return
        removeState: (className) ->
            data = $rootScope.$storage[storageKeyName]
            if not data then return
            data = WordChecker.removeWord data, className
            $rootScope.$storage[storageKeyName] = angular.toJson data
            return
        restoreState: ($elem) ->
            data = angular.fromJson $rootScope.$storage[storageKeyName]
            if not data then return
            $elem.addClass data
            return
    }
