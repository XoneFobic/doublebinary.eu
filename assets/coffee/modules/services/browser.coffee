spectre.service 'browser', ->
    'use strict'
    uaMatch = (ua) ->
        ua = ua.toLowerCase()
        match = /(opr)[\/]([\w.]+)/.exec(ua) or /(chrome)[ \/]([\w.]+)/.exec(ua) or
                /(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec(ua) or /(webkit)[ \/]([\w.]+)/.exec(ua) or
                /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) or /(msie) ([\w.]+)/.exec(ua) or
                ua.indexOf('trident') >= 0 and /(rv)(?::| )([\w.]+)/.exec(ua) or
                ua.indexOf('compatible') < 0 and /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) or []
        platform_match = /(ipad)/.exec(ua) or /(iphone)/.exec(ua) or /(android)/.exec(ua) or
                /(windows phone)/.exec(ua) or /(win)/.exec(ua) or /(mac)/.exec(ua) or /(linux)/.exec(ua) or
                /(cros)/i.exec(ua) or []
        {
        browser : match[3] or match[1] or ''
        version : match[2] or '0'
        platform: platform_match[0] or ''
        }

    matched = uaMatch(window.navigator.userAgent)
    browser = {}
    if matched.browser
        browser[matched.browser] = true
        browser.version = matched.version
        browser.versionNumber = parseInt(matched.version)
    if matched.platform
        browser[matched.platform] = true
    if browser.android or browser.ipad or browser.iphone or browser['windows phone']
        browser.mobile = true
    if browser.cros or browser.mac or browser.linux or browser.win
        browser.desktop = true
    if browser.chrome or browser.opr or browser.safari
        browser.webkit = true
    if browser.rv
        ie = 'msie'
        matched.browser = ie
        browser[ie] = true
    if browser.opr
        opera = 'opera'
        matched.browser = opera
        browser[opera] = true
    if browser.safari and browser.android
        android = 'android'
        matched.browser = android
        browser[android] = true
    browser.name = matched.browser
    browser.platform = matched.platform
    browser
