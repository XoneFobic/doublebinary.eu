spectre.service 'Utils', ($window, APP_MEDIAQUERY) ->
    'use strict'
    $html = angular.element 'html'
    $win = angular.element $window
    $body = angular.element 'body'
    {
    support           :
        transition           : do ->
            transitionEnd = do ->
                element = document.body or document.documentElement
                transEndEventNames =
                    WebkitTransition: 'webkitTransitionEnd'
                    MozTransition   : 'transitionend'
                    OTransition     : 'oTransitionEnd otransitionend'
                    transition      : 'transitionend'
                name = undefined
                for name of transEndEventNames
                    name = name
                    if element.style[name] isnt undefined
                        return transEndEventNames[name]
                return
            transitionEnd and end: transitionEnd
        animation            : do ->
            animationEnd = do ->
                element = document.body or document.documentElement
                animEndEventNames =
                    WebkitAnimation: 'webkitAnimationEnd'
                    MozAnimation   : 'animationend'
                    OAnimation     : 'oAnimationEnd oanimationend'
                    animation      : 'animationend'
                name = undefined
                for name of animEndEventNames
                    name = name
                    if element.style[name] isnt undefined
                        return animEndEventNames[name]
                return
            animationEnd and end: animationEnd
        requestAnimationFrame:
            window.requestAnimationFrame or window.webkitRequestAnimationFrame or window.mozRequestAnimationFrame or
            window.msRequestAnimationFrame or window.oRequestAnimationFrame or (callback) ->
                window.setTimeout callback, 1000 / 60
                return
        touch                : 'ontouchstart' in window and navigator.userAgent.toLowerCase().match(/mobile|tablet/) or
            window.DocumentTouch and document instanceof window.DocumentTouch or
            window.navigator['msPointerEnabled'] and window.navigator['msMaxTouchPoints'] > 0 or
            window.navigator['pointerEnabled'] and window.navigator['maxTouchPoints'] > 0 or false
        mutationobserver     : window.MutationObserver or window.WebKitMutationObserver or
            window.MozMutationObserver or null
    isInView          : (element, options) ->
        $element = angular.element element
        if not $element.is(':visible')
            return false
        window_left = $win.scrollLeft()
        window_top = $win.scrollTop()
        offset = $element.offset()
        left = offset.left
        top = offset.top
        options = $.extend({
            topoffset : 0
            leftoffset: 0
        }, options)
        if top + $element.height() >= window_top and top - options.topoffset <= window_top + $win.height() and
        left + $element.width() >= window_left and left - options.leftoffset <= window_left + $win.width()
            true
        else
            false
    langdirection     : if $html.attr('dir') is 'rtl' then 'right' else 'left'
    isTouch           : ->
        $html.hasClass 'touch'
    isSidebarCollapsed: ->
        $body.hasClass 'aside-collapsed'
    isSidebarToggled  : ->
        $body.hasClass 'aside-toggled'
    isMobile          : ->
        $win.width() < APP_MEDIAQUERY.tablet
    }
