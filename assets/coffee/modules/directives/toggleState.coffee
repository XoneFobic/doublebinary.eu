spectre.directive 'toggleState', (ToggleStateService) ->
    'use strict'
    {
        restrict: 'A'
        link: (scope, element, attributes) ->
            $body = angular.element 'body'

            $(element).on 'click', (e) ->
                e.preventDefault()
                className = attributes.toggleState

                if className
                    if $body.hasClass className
                        $body.removeClass className
                        if not attributes.noPersist
                            ToggleStateService.removeState className
                            return
                    else
                        $body.addClass className
                        if not attributes.noPersist
                            ToggleStateService.addState className
                            return
    }
