spectre
.config ($stateProvider, $locationProvider, $urlRouterProvider, RouteHelpersProvider) ->
    'use strict'
    $locationProvider.html5Mode false
    $urlRouterProvider.otherwise '/home'
    $stateProvider
    .state 'spectre',
        url        : ''
        abstract   : true
        templateUrl: RouteHelpersProvider.basepath 'app.html'
        controller : 'AppController'
        resolve    : RouteHelpersProvider.resolveFor 'modernizr', 'icons'
    .state 'spectre.home',
        url        : '/home'
        title      : 'Home'
        templateUrl: RouteHelpersProvider.basepath 'home.html'
    return
.config ($ocLazyLoadProvider, APP_REQUIRES) ->
    'use strict'
    $ocLazyLoadProvider.config
        debug  : false
        events : true
        modules: APP_REQUIRES.modules
    return
.config ($controllerProvider, $compileProvider, $filterProvider, $provide) ->
    'use strict'
    spectre.controller = $controllerProvider.register
    spectre.directive = $compileProvider.directive
    spectre.filter = $filterProvider.register
    spectre.factory = $provide.factory
    spectre.service = $provide.service
    spectre.constant = $provide.constant
    spectre.value = $provide.value
    return
.config ($translateProvider) ->
    $translateProvider.useStaticFilesLoader
        prefix: 'i18n/'
        suffix: '.json'
    $translateProvider.preferredLanguage 'nl'
    $translateProvider.useLocalStorage()
    $translateProvider.usePostCompiling true
    return
.config (cfpLoadingBarProvider) ->
    cfpLoadingBarProvider.includeBar = true
    cfpLoadingBarProvider.includeSpinner = true
    cfpLoadingBarProvider.latencyThreshold = 500
    cfpLoadingBarProvider.parentSelector = '.wrapper > section'
    return
.config ($tooltipProvider) ->
    $tooltipProvider.options appendToBody: true
    return
