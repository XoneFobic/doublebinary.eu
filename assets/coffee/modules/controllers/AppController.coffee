spectre.controller 'AppController', ($rootScope, $scope, $state, $translate, $window, $localStorage, $timeout,
                                     ToggleStateService, colours, browser, cfpLoadingBar) ->
    'use strict'
    thBar = null
    $rootScope.$on '$stateChangeStart', (event, toState, toParams, fromState, fromParams) ->
        if angular.element('.wrapper > section').length
            thBar = $timeout ->
                cfpLoadingBar.start()
                return
            , 0
        return
    $rootScope.$on '$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) ->
        event.targetScope.$watch '$viewContentLoaded', ->
            $timeout.cancel thBar
            cfpLoadingBar.complete()
            return
        return
    $rootScope.$on '$stateNotFound', (event, notFoundState, fromState, fromParams) ->
#        console.error notFoundState.to
#        console.error notFoundState.toParams
#        console.error notFoundState.options
        return
    $rootScope.$on '$stateChangeError', (event, toState, toParams, fromState, fromParams, error) ->
#        console.error error
        return
    $rootScope.$on '$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) ->
        $window.scrollTo 0, 0
        $rootScope.currTitle = $state.current.title
        return
    $rootScope.currTitle = $state.current.title
    $rootScope.pageTitle = ->
        title = $rootScope.spectre.title + ' - ' + ($rootScope.currTitle or $rootScope.spectre.description)
        document.title = title
        title
    $rootScope.$watch 'spectre.layout.isCollapsed', (newValue, oldValue) ->
        if newValue is false
            $rootScope.$broadcast 'closeSidebarMenu'
        return
    if angular.isDefined $localStorage.layout
        $scope.spectre.layout = $localStorage.layout
    else
        $localStorage.layout = $scope.spectre.layout
    $rootScope.$watch 'spectre.layout', ->
        $localStorage.layout = $scope.spectre.layout
        return
    , true
    $scope.colourByName = colours.byName
    $scope.language =
        listIsOpen: false
        available :
            'en': 'English'
            'nl': 'Nederlands'
        init      : ->
            proposedLanguage = $translate.proposedLanguage() or $translate.use()
            preferredLanguage = $translate.preferredLanguage()
            $scope.language.selected = $scope.language.available[proposedLanguage or preferredLanguage]
            return
        set       : (localeId, ev) ->
            $translate.use localeId
            $scope.language.selected = $scope.language.available[localeId]
            $scope.language.listIsOpen = not $scope.language.listIsOpen
            return
    $scope.language.init()
    ToggleStateService.restoreState angular.element('body')
    $rootScope.cancel = ($event) ->
        $event.stopPropagation()
        return
    return
