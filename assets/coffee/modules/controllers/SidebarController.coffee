spectre.controller 'SidebarController', ($rootScope, $scope, $state, $http, $timeout, Utils) ->
    collapseList = []
    closeAllBut = (index) ->
        index += ''
        for i of collapseList
            if index < 0 or index.indexOf(i) < 0
                collapseList[i] = true
        return
    isChild = ($index) ->
        typeof $index is 'string' and not ($index.indexOf('-') < 0)
    $rootScope.$watch 'spectre.layout.asideHover', (oldVal, newVal) ->
        if newVal is false and oldVal is true
            closeAllBut -1
        return
    isActive = (item) ->
        if not item
            return
        if not item.sref or item.sref is '#'
            foundActive = false
            angular.forEach item.submenu, (value, key) ->
                if isActive(value) then foundActive = true
                return
            foundActive
        else
            $state.is(item.sref) or $state.includes(item.sref)
    $scope.getMenuItemPropClasses = (item) ->
        (if item.heading then 'nav-heading' else '') + (if isActive(item) then ' active' else '')
    $scope.loadSidebarMenu = ->
        menuJson = 'api/sidebar-menu.json'
        menuURL = menuJson + '?v=' + (new Date).getTime()
        $http.get(menuURL).success((items) ->
            $scope.menuItems = items
            return
        ).error (data, status, headers, config) ->
            alert 'Failure loading menu'
            return
        return
    $scope.loadSidebarMenu()
    $scope.addCollapse = ($index, item) ->
        collapseList[$index] = if $rootScope.spectre.layout.asideHover then true else not isActive(item)
        return
    $scope.isCollapse = ($index) ->
        collapseList[$index]
    $scope.toggleCollapse = ($index, isParentItem) ->
        if Utils.isSidebarCollapsed() or $rootScope.spectre.layout.asideHover
            return true
        if angular.isDefined collapseList[$index]
            if not $scope.lastEventFromChild
                collapseList[$index] = not collapseList[$index]
                closeAllBut $index
        else if isParentItem
            closeAllBut -1
        $scope.lastEventFromChild = isChild $index
        true
    return
