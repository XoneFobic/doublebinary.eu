spectre = angular.module 'spectre', [
    'ngRoute'
    'ngAnimate'
    'ngStorage'
    'ngCookies'
    'pascalprecht.translate'
    'ui.bootstrap'
    'ui.router'
    'oc.lazyLoad'
    'cfp.loadingBar'
    'ngSanitize'
    'ngResource'
    'ui.utils'
]

spectre.run ($rootScope, $state, $stateParams, $window) ->
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams
    $rootScope.$storage = $window.localStorage
    $rootScope.spectre =
        title        : 'Spectre'
        description  : 'Blog, Portfolio, Resume and more'
        layout       :
            isFixed    : true,
            isCollapsed: false,
            isBoxed    : false,
            isRTL      : false,
            horizontal : false,
            isFloat    : false,
            asideHover : false,
            theme      : 'doublebinary'
        useFullLayout: false
        hiddenFooter : false
        viewAnimation: 'ng-fadeInUp'
    $rootScope.user =
        name: 'Dennis Brandt'
        job : 'Team Leader / Web-developer'
    return
